# Swift Lint usage

This is a fast guide, for use `Swift Lint` in your Swift project, for achieve an ordered and cleaner code.

## Include Swift Lint Pod Library

First step is add `SwiftLint` pod in your project `Podfile`,

```ruby
pod 'SwiftLint'
```

Then install your pod:

```ruby
pod install
```

## Integrate on XCode

Once you have your `SwiftLint` pod installed, you need to add a script into your "Scheme", in `Build Phases` add the script under the `Run Script` option:

```ruby
"${PODS_ROOT}/SwiftLint/swiftlint"
```

As shown below:

![AddScript Example](https://s3.amazonaws.com/shareproximateapps/doc/ios/5.png)

Now in your next build, `SwiftLint` must be work inspecting your code, displaying warnings/errors according to your `SwiftLint` rules, by default `SwiftLint` has a few rules. If everything goes all right, XCode use the `SwiftLint` rules each build:

![RuleViolation1 Example](https://s3.amazonaws.com/shareproximateapps/doc/ios/8.png)

![RuleViolation2 Example](https://s3.amazonaws.com/shareproximateapps/doc/ios/6.png)

![RuleViolation3 Example](https://s3.amazonaws.com/shareproximateapps/doc/ios/7.png)

### Integrate on a develop Pod

If you want to use `SwiftLint` on your Pod library, you need to add the Script from the Pod project,

Go to `Pods` project in your workspace:

![PodIntegration1 Example](https://s3.amazonaws.com/shareproximateapps/doc/android/lint_2.png)

Then select your `Target`,

![PodIntegration2 Example](https://s3.amazonaws.com/shareproximateapps/doc/android/lint_3.png)

and in their `Build Phase` add a `New Run Script Phase`,

![PodIntegration3 Example](https://s3.amazonaws.com/shareproximateapps/doc/android/lint_4.png)

add this script with your respective `library` path:

```ruby
"${PODS_ROOT}/SwiftLint/swiftlint" --path ../../library/Classes/
```

![PodIntegration4 Example](https://s3.amazonaws.com/shareproximateapps/doc/android/lint_1.png)

The only difference adding the script in a develop Pod is adding the Pod´s source code path.

## Swift Lint Command Utilities

You can use `swiftlint` command for use some utilities that `SwiftLint` allow us, from the Pod installation path `Pods/SwiftLint/swiftlint`:

```ruby
$ swiftlint help
Available commands:

   autocorrect  Automatically correct warnings and errors
   help         Display general or command-specific help
   lint         Print lint warnings and errors for the Swift files in the current directory (default command)
   rules        Display the list of rules and their identifiers
   version      Display the current version of SwiftLint
```

That´s it, your project source code are ready to be inspected!
